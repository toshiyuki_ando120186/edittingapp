//
//  UIView+Image.swift
//  Photo Editor
//
//  Created by Mohamed Hamed on 4/23/17.
//  Copyright © 2017 Mohamed Hamed. All rights reserved.
//

import UIKit

extension UIView {
    /**
     Convert UIView to UIImage
     */
    func toImage() -> UIImage {
      //  UIGraphicsBeginImageContextWithOptions(self.bounds.size, self.isOpaque, 0.0)
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, 0.0)
        self.drawHierarchy(in: self.bounds, afterScreenUpdates: false)
        let snapshotImageFromMyView = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return snapshotImageFromMyView!
        
    }
    
    func toImageBrush(x_max : CGFloat , y_max: CGFloat, x_min : CGFloat , y_min : CGFloat ) -> UIImage {
        //  UIGraphicsBeginImageContextWithOptions(self.bounds.size, self.isOpaque, 0.0)
        UIGraphicsBeginImageContextWithOptions(CGSize(width: x_max - x_min, height: y_max - y_min), false, 0.0)
        self.drawHierarchy(in: CGRect(x: x_min, y: y_min, width: x_max - x_min, height: y_max - y_min ), afterScreenUpdates: false)
        let snapshotImageFromMyView = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return snapshotImageFromMyView!
        
    }
}
