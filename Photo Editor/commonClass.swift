//
//  commonClass.swift
//  Photo Editor
//
//  Created by zhezui on 2019/1/20.
//  Copyright © 2019 Mohamed Hamed. All rights reserved.
//

import UIKit
import Foundation

//let instance = commonClass()
var tag_val = 1000
let index_String = "$"
var x_max : CGFloat = 0.0
var y_max : CGFloat = 0.0
var x_min : CGFloat = 0.0
var y_min : CGFloat = 0.0
var brushWidth : CGFloat = 4.0
var panHeight : CGFloat = 200
var offsetHeight : CGFloat = 40


class commonClass : NSObject {

  public  var objectTag : Int = 1000
    
    func GotoViewController(destViewID: String)
    {
        
        let appDelegateTemp = UIApplication.shared.delegate as? AppDelegate
        let storyboard = UIStoryboard(name: "PhotoEditor" , bundle: Bundle.main)
        let mainVcIntial = storyboard.instantiateViewController(withIdentifier:destViewID)
        //let mainController = UINavigationController(rootViewController: mainVcIntial)
        appDelegateTemp?.window?.rootViewController = mainVcIntial
        appDelegateTemp?.window?.makeKeyAndVisible()
        
    }
    
    
}
