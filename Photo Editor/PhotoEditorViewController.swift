//
//  ViewController.swift
//  Photo Editor
//
//  Created by Mohamed Hamed on 4/23/17.
//  Copyright © 2017 Mohamed Hamed. All rights reserved.
//

import UIKit
import Photos
import VerticalSteppedSlider

class PhotoEditorViewController: UIViewController {

  
    @IBOutlet weak var brushImageview: UIImageView!
    @IBOutlet weak var strAAA: UIImageView!
    @IBOutlet weak var canvasView: UIView!
 
    @IBOutlet weak var cursorView: UIImageView!
    @IBOutlet var imageView: UIImageView!
    @IBOutlet weak var imageViewHeightConstraint: NSLayoutConstraint!
   
    @IBOutlet weak var canvasImageView: UIImageView!

    @IBOutlet weak var topToolbar: UIView!
    @IBOutlet weak var bottomToolbar: UIView!

    @IBOutlet weak var topGradient: UIView!
    @IBOutlet weak var bottomGradient: UIView!
    
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var deleteView: UIView!
    @IBOutlet weak var colorsCollectionView: UICollectionView!
    @IBOutlet weak var colorPickerView: UIView!
    @IBOutlet weak var colorPickerViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var brushPickerViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var brushHeightViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var textStyleView: UIView!
    @IBOutlet weak var brushView: UIView!
    
    @IBOutlet weak var brushThicknessSlider: UISlider!
    
    //Controls
    @IBOutlet weak var cropButton: UIButton!
    @IBOutlet weak var stickerButton: UIButton!
    @IBOutlet weak var drawButton: UIButton!
    @IBOutlet weak var textButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var clearButton: UIButton!
    
    @IBOutlet weak var dotLineViewTop: UIView!
    @IBOutlet weak var dotlineViewBottom: UIView!
    
    
    public var image: UIImage?
    public var imageSelected: UIImage?
   
    public var stickers = [PHAsset]()
    
    public var colors  : [UIColor] = []
    
    public var photoEditorDelegate: PhotoEditorDelegate?
    var colorsCollectionViewDelegate: ColorsCollectionViewDelegate!
    
   
    public var hiddenControls : [control] = []
    
    var stickersVCIsVisible = false
    var drawColor: UIColor = UIColor.black
    var textColor: UIColor = UIColor.blue
    var isDrawing: Bool = false
    var lastPoint: CGPoint!
    var swiped = false
    var lastPanPoint: CGPoint?
    var lastTextViewTransform: CGAffineTransform?
    var lastTextViewTransCenter: CGPoint?
    var lastTextViewFont:UIFont?
    var activeTextView: UITextView?
    var imageViewToPan: UIImageView?
    var isTyping: Bool = false
    
    
    var stickersViewController: StickersViewController!
    public var currentlyEditingLabel: JLStickerLabelView!
    public var currentlyEditingText: UITextView!
    var labels: NSMutableArray!
    //var dottedBorder = CAShapeLayer()

    public override func loadView() {
        registerFont()
        super.loadView()
        
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        image = UIImage(named: "tes")
        labels = []
        deleteView.layer.cornerRadius = deleteView.bounds.height / 2
        deleteView.layer.borderWidth = 2.0
        deleteView.layer.borderColor = UIColor.black.cgColor
        deleteView.clipsToBounds = true
        
        topVSSlider.maximumValue = Float(dotLineViewTop.frame.height)
        topVSSlider.minimumValue = 10
        bottomVSSlider.maximumValue = Float(dotlineViewBottom.frame.height )
        bottomVSSlider.minimumValue = 10
        topVSSlider.value = Float(dotLineViewTop.frame.height ) - 40
        bottomVSSlider.value = 40

        panHeight = self.canvasView.frame.height - 75
        offsetHeight = 40
        drawDottedLine(start: CGPoint(x: 0, y: 45), end: CGPoint(x: dotLineViewTop.frame.width, y: 45), view: dotLineViewTop)
        drawDottedLine(start: CGPoint(x: 0, y: dotlineViewBottom.frame.height - 40), end: CGPoint(x: dotlineViewBottom.frame.width, y: dotlineViewBottom.frame.height - 40), view: dotlineViewBottom)

        
        let edgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(screenEdgeSwiped))
        edgePan.edges = .bottom
        edgePan.delegate = self
        self.view.addGestureRecognizer(edgePan)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidShow),
                                               name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide),
                                               name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self,selector: #selector(keyboardWillChangeFrame(_:)),
                                               name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        canvasImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(remove)))
        
        
        configureCollectionView()
        stickersViewController = StickersViewController(nibName: "StickersViewController", bundle: Bundle(for: StickersViewController.self))
        configureBottomMenu()
        getImages()
        
    }
    @objc func remove(){
        for subview in canvasImageView.subviews {
            if subview is UIButton {
                subview.removeFromSuperview()
            }
            if let sublayers = subview.layer.sublayers {
                for sublayer in sublayers{
                    if sublayer is CAShapeLayer{
                        sublayer.removeFromSuperlayer()
                    }
                }
            }
        }
        view.endEditing(true)
        doneButton.isHidden = true
        colorPickerView.isHidden = true
        brushView.isHidden = true
        textStyleView.isHidden = true
        canvasImageView.isUserInteractionEnabled = true
        hideToolbar(hide: false)
        isDrawing = false
    }
    
    @IBOutlet weak var bottomVSSlider: VSSlider!
    
    @IBOutlet weak var topVSSlider: VSSlider!
    @IBAction func topVSSliderAction(_ sender: Any) {
        offsetHeight = dotLineViewTop.frame.height - CGFloat(topVSSlider.value)
        panHeight = CGFloat(topVSSlider.value) + dotLineViewTop.frame.height - CGFloat(bottomVSSlider.value)
        let firstPoint = CGPoint(x: 0, y: (dotLineViewTop.frame.height - CGFloat(topVSSlider.value)))
        let lastPoint = CGPoint(x: dotLineViewTop.frame.width, y: (dotLineViewTop.frame.height - CGFloat(topVSSlider.value)))
       print(topVSSlider.value) //topVSSlider.value
        if let sublayers = dotLineViewTop.layer.sublayers {
            for sublayer in sublayers{
                if sublayer is CAShapeLayer{
                    sublayer.removeFromSuperlayer()
                    drawDottedLine(start: firstPoint, end: lastPoint, view: dotLineViewTop)
                }
            }
        }
        
    }
    
    @IBAction func bottomVSSliderAction(_ sender: Any) {
        print(bottomVSSlider.value)
        panHeight = CGFloat(topVSSlider.value) + dotLineViewTop.frame.height - CGFloat(bottomVSSlider.value)
        let firstPoint = CGPoint(x: 0, y: (dotlineViewBottom.frame.height - CGFloat(bottomVSSlider.value)))
        let lastPoint = CGPoint(x: dotlineViewBottom.frame.width, y: (dotlineViewBottom.frame.height - CGFloat(bottomVSSlider.value)))
       // print(topVSSlider.value) //topVSSlider.value
        if let sublayers = dotlineViewBottom.layer.sublayers {
            for sublayer in sublayers{
                if sublayer is CAShapeLayer{
                    sublayer.removeFromSuperlayer()
                    drawDottedLine(start: firstPoint, end: lastPoint, view: dotlineViewBottom)
                }
            }
        }
        
    }
    func drawDottedLine(start p0: CGPoint, end p1: CGPoint, view: UIView) {
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = UIColor.lightGray.cgColor
        shapeLayer.lineWidth = 3
        shapeLayer.lineDashPattern = [7, 3] // 7 is the length of dash, 3 is length of the gap.
        
        let path = CGMutablePath()
        path.addLines(between: [p0, p1])
        shapeLayer.path = path
        view.layer.addSublayer(shapeLayer)
    }
    
    @IBAction func BrushTickChange(_ sender: Any) {
        
        brushWidth = CGFloat(self.brushThicknessSlider.value)
    }
    
    func getImages() {
        let assets = PHAsset.fetchAssets(with: PHAssetMediaType.image, options: nil)
        assets.enumerateObjects({ (object, count, stop) in
            self.stickers.append(object)
        })
        
        self.stickers.reverse()
        
    }
    
    func configureBottomMenu(){
        self.textButton.setImage(UIImage(named: "IconTextFilled"), for: .selected)
        self.textButton.setImage(UIImage(named: "IconText"), for: .normal)
        
        self.drawButton.setImage(UIImage(named: "IconBrushFilled"), for: .selected)
        self.drawButton.setImage(UIImage(named: "IconBrush"), for: .normal)
        
        self.stickerButton.setImage(UIImage(named: "IconStickerFilled"), for: .selected)
        self.stickerButton.setImage(UIImage(named: "IconSticker"), for: .normal)
    }
    
    func configureCollectionView() {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: 30, height: 30)
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        colorsCollectionView.collectionViewLayout = layout
        colorsCollectionViewDelegate = ColorsCollectionViewDelegate()
        colorsCollectionViewDelegate.colorDelegate = self
        if !colors.isEmpty {
            colorsCollectionViewDelegate.colors = colors
        }
        colorsCollectionView.delegate = colorsCollectionViewDelegate
        colorsCollectionView.dataSource = colorsCollectionViewDelegate
        
        colorsCollectionView.register(
            UINib(nibName: "ColorCollectionViewCell", bundle: Bundle(for: ColorCollectionViewCell.self)),
            forCellWithReuseIdentifier: "ColorCollectionViewCell")
    }
    
    func setImageView(image: UIImage) {
        imageView.image = image
        let size = image.suitableSize(widthLimit: UIScreen.main.bounds.width)
        imageViewHeightConstraint.constant = (size?.height)!
    }
    
    func hideToolbar(hide: Bool) {
        topToolbar.isHidden = false
      //  topGradient.isHidden = true
        bottomToolbar.isHidden = hide
        bottomGradient.isHidden = hide
    }
}

extension PhotoEditorViewController: ColorDelegate {
    func didSelectColor(color: UIColor) {
        if isDrawing {
            self.drawColor = color
            
        } else if currentlyEditingText != nil {
            currentlyEditingText.textColor = color
            textColor = color
        }
    }
}






