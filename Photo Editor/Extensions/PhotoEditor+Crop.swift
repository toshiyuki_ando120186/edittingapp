//
//  PhotoEditor+Crop.swift
//  Pods
//
//  Created by Mohamed Hamed on 6/16/17.
//
//

import Foundation
import UIKit

// MARK: - CropView
extension PhotoEditorViewController: CropViewControllerDelegate {
    
    public func cropViewController(_ controller: CropViewController, didFinishCroppingImage image: UIImage, transform: CGAffineTransform, cropRect: CGRect) {
   

        self.removeStickersView()
        self.imageSelected = image
        
        for imageView in subImageViews(view: canvasImageView){
            if imageView.tag == cropViewTag {
                imageView.image = image
            }
        }
        controller.dismiss(animated: true, completion: nil)
    }
    
    public func cropViewControllerDidCancel(_ controller: CropViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
}
