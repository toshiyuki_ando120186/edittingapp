//
//  PhotoEditor+Controls.swift
//  Pods
//
//  Created by Mohamed Hamed on 6/16/17.
//
//

import Foundation
import UIKit
var FinalData = ""
var textViewWidth = UIScreen.main.bounds.size.width - 100
// MARK: - Control
public enum control {
    case crop
    case sticker
    case draw
    case text
    case save
    case share
    case clear
}

extension PhotoEditorViewController {

     //MARK: Top Toolbar
    
    @IBAction func cancelButtonTapped(_ sender: Any) {
        photoEditorDelegate?.canceledEditing()
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func cropButtonTapped(_ sender: UIButton ) {
        let controller = CropViewController()
        controller.delegate = self
        controller.image = self.imageSelected
        let navController = UINavigationController(rootViewController: controller)
        present(navController, animated: true, completion: nil)
    }

    @IBAction func stickersButtonTapped(_ sender: Any) {
        isDrawing = false
        colorPickerView.isHidden = true
        brushView.isHidden = true
        self.selectedControls(false, draw: false, stickers: true)
        addStickersViewController()
    }

    @IBAction func drawButtonTapped(_ sender: Any) {
        isDrawing = true
        self.brushHeightViewConstraint.constant = 50.0
        self.brushView.isHidden = false
        self.textStyleView.isHidden = true
        canvasImageView.isUserInteractionEnabled = false
        doneButton.isHidden = false
        colorPickerView.isHidden = false
        self.selectedControls(false, draw: true, stickers: false)
        hideToolbar(hide: false)
    }

    @IBAction func textButtonTapped(_ sender: Any) {
        isTyping = true
        colorPickerView.isHidden = false
        self.brushView.isHidden = true
        self.textStyleView.isHidden = false
        self.brushHeightViewConstraint.constant = 0.0
        self.selectedControls(true, draw: false, stickers: false)
        let textEditView = UITextView(frame: CGRect(x: 50, y: canvasImageView.center.y,
                                                    width: UIScreen.main.bounds.width - 100, height: 50))
        textEditView.backgroundColor = UIColor.clear
        textEditView.font = UIFont(name: "Helvetica", size: 25)
        textEditView.textAlignment = .center
        textEditView.textColor = self.textColor
        textEditView.textContainer.lineBreakMode = .byWordWrapping
        textEditView.isScrollEnabled = false
        textEditView.delegate = self
        self.canvasImageView.addSubview(textEditView)
        textEditView.becomeFirstResponder()
        addGestures(view: textEditView)
        textEditView.tag = tag_val
        let val1 : CGFloat = 0.0
        rotation_info[String(tag_val)] = val1
        widthInfo[String(tag_val)] = textEditView.frame.width
        heightInfo[String(tag_val)] = textEditView.frame.height
        scale_info[String(tag_val)] = 1.0
        tag_val = tag_val + 1
        
        self.currentlyEditingText = textEditView
      
    }
    
    @IBAction func doneButtonTapped(_ sender: Any) {
        view.endEditing(true)
        doneButton.isHidden = true
        colorPickerView.isHidden = true
        brushView.isHidden = true
        textStyleView.isHidden = true
        canvasImageView.isUserInteractionEnabled = true
        hideToolbar(hide: false)
        isDrawing = false
    }
    
    //MARK: Bottom Toolbar
    
    @IBAction func saveButtonTapped(_ sender: AnyObject) {

        UIImageWriteToSavedPhotosAlbum(canvasView.toImage(),self, #selector(PhotoEditorViewController.image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    
    @IBAction func shareButtonTapped(_ sender: UIButton) {
        let activity = UIActivityViewController(activityItems: [canvasView.toImage()], applicationActivities: nil)
        present(activity, animated: true, completion: nil)
        
    }
    
    @IBAction func clearButtonTapped(_ sender: AnyObject) {
      
        canvasImageView.image = nil
        for subview in canvasImageView.subviews {
            subview.removeFromSuperview()
        }
    }
    
    @IBAction func continueButtonPressed(_ sender: Any) {

        self.view.endEditing(true)
        for recognizer in canvasImageView.gestureRecognizers ?? [] {
            canvasImageView.removeGestureRecognizer(recognizer)
        }
        if FinalData != "" {
            FinalData = FinalData.dropLast() + "#"
        }
        let count = tag_val - 1000
        let loop = 1000
        for i in 0 ..< count {
            
            if let saveImage =  self.canvasImageView.findSubview(withTag: 1000 + i)
            {
                let imageData  = saveImage.toImage().pngData() as! NSData
                let strBase64 = imageData.base64EncodedString(options: .lineLength76Characters)
                let additional = index_String + String(Float(saveImage.center.x)) + index_String + String(Float(saveImage.center.y - offsetHeight)) + index_String + String(Float(heightInfo[String(1000 + i)]!)) + index_String + String(Float(widthInfo[String(1000 + i)]!)) + index_String
                    + String(Float(rotation_info[String(1000 + i)]!))  + index_String +  String(Float(scale_info[String(1000 + i)]!)) + index_String +  String(Float( panHeight )) + index_String
                FinalData = FinalData + strBase64 + additional
            }
        }
       
         commonClass().GotoViewController(destViewID: "EEpinch")
    
    }

    @objc func image(_ image: UIImage, didFinishSavingWithError error: NSError?, contextInfo: UnsafeRawPointer) {
        var title = ""
        var message = ""
        if let error = error {
            title = "Image not saved!"
            message = error.localizedDescription
        } else {
            title = "Image Saved"
            message = "Image successfully saved to Photos library"
        }
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func selectedControls(_ text: Bool, draw: Bool, stickers: Bool){
        self.drawButton.isSelected = draw
        self.stickerButton.isSelected = stickers
        self.textButton.isSelected = text
    }
    
    func hideControls() {
        for control in hiddenControls {
            switch control {
                
            case .clear:
                clearButton.isHidden = true
            case .crop:
                cropButton.isHidden = true
            case .draw:
                drawButton.isHidden = true
            case .save:
                saveButton.isHidden = true
            case .share:
                shareButton.isHidden = true
            case .sticker:
                stickerButton.isHidden = true
            case .text:
                stickerButton.isHidden = true
            }
        }
    }
    
}
extension UIView {
    func findSubview(withTag tag: Int) -> UIView? {
        for subview in self.subviews {
            if subview.tag == tag {
                return subview
            }
        }
        return nil
    }
}
