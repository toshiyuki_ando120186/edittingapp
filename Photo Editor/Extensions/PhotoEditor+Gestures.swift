//
//  PhotoEditor+Gestures.swift
//  Photo Editor
//
//  Created by Mohamed Hamed on 6/16/17.
//
//

import Foundation


import UIKit
var deltaSum : CGFloat = 0.0
var dd : CGFloat = 0.0
var rotation_info = [ String : CGFloat]()
var scale_info = [ String : CGFloat]()
var widthInfo = [ String : CGFloat]()
var heightInfo = [ String : CGFloat]()
var hei_label = [ String : Int]()
var zoom : CGFloat = 1.0


var cropViewTag : Int!

//var rotationArray : [rotation_info]!
extension PhotoEditorViewController : UIGestureRecognizerDelegate  {
    
    /**
     UIPanGestureRecognizer - Moving Objects
     Selecting transparent parts of the imageview won't move the object
     */
    @objc func panGesture(_ recognizer: UIPanGestureRecognizer) {
        
        
        
        if let view = recognizer.view {
            
            for subview in canvasImageView.subviews {
                if subview is UIButton {
                    subview.removeFromSuperview()
                }
            }
            
            if view is UIImageView {
                //Tap only on visible parts on the image
                if recognizer.state == .began {
                    for imageView in subImageViews(view: canvasImageView) {
                        let location = recognizer.location(in: imageView)
                        let alpha = imageView.alphaAtPoint(location)
                        if alpha > 0 {
                            imageViewToPan = imageView
                            break
                        }
                    }
                }
                if imageViewToPan != nil {
                    moveView(view: imageViewToPan!, recognizer: recognizer)
                   // print(recognizer.location(in: canvasView))
                }
            }
           
            else {
                moveView(view: view, recognizer: recognizer)
               // print(recognizer.location(in: canvasView))
            }
        }
    }
    
    /**
     UIPinchGestureRecognizer - Pinching Objects
     If it's a UITextView will make the font bigger so it doen't look pixlated
     */
    @objc func pinchGesture(_ recognizer: UIPinchGestureRecognizer) {
        
        for subview in canvasImageView.subviews {
            if subview is UIButton {
                subview.removeFromSuperview()
            }
        }
        
        if let view = recognizer.view {
            if view is UITextView {
                let textView = view as! UITextView
               
                textView.transform = textView.transform.scaledBy(x: recognizer.scale, y: recognizer.scale)
                zoom = scale_info[String(textView.tag)]!
                zoom = zoom * recognizer.scale
                scale_info[String(textView.tag)] = zoom
                textView.layoutIfNeeded()
                textView.setNeedsDisplay()
            }
            
            else {
                view.transform = view.transform.scaledBy(x: recognizer.scale, y: recognizer.scale)
                zoom = scale_info[String(view.tag)]!
                zoom = zoom * recognizer.scale
                scale_info[String(view.tag)] = zoom
            }
            recognizer.scale = 1
        }
    }
    
    
 
    
    @objc func rotationGesture(_ recognizer: UIRotationGestureRecognizer) {
        
        for subview in canvasImageView.subviews {
            if subview is UIButton {
                subview.removeFromSuperview()
            }
        }
        if let view = recognizer.view {
            if view is UIButton {
                view.removeFromSuperview()
            }
            view.transform = view.transform.rotated(by: recognizer.rotation)
            if recognizer.rotation != 0 {
                print("rotation value is")
                deltaSum = rotation_info[String(view.tag)]!
                deltaSum = deltaSum + recognizer.rotation
                rotation_info[String(view.tag)] = deltaSum
            }
         
            recognizer.rotation = 0
        }
    }
   
    @objc func tapGesture(_ recognizer: UITapGestureRecognizer) {
        
        for subview in canvasImageView.subviews {
            if subview is UIButton {
                subview.removeFromSuperview()
            }
            if let sublayers = subview.layer.sublayers {
                for sublayer in sublayers{
                    if sublayer is CAShapeLayer{
                        sublayer.removeFromSuperlayer()
                    }
                }
            }
        }
        if let view = recognizer.view {
            if view is UIImageView {
              
                for imageView in subImageViews(view: canvasImageView) {
                    let location = recognizer.location(in: imageView)
                    let alpha = imageView.alphaAtPoint(location)
                    if alpha > 0 {
                        
                        if view is UIButton {
                            view.removeFromSuperview()
                        }
                        
                        cropViewTag = imageView.tag
                        let  ImagecropButton  = UIButton(frame: CGRect(x:imageView.frame.origin.x + imageView.frame.width, y:imageView.frame.origin.y - 20, width: 20, height: 20))
                      
                        canvasImageView.addSubview(ImagecropButton)
                        ImagecropButton.setImage(UIImage(named: "iconfinder_interface-79_809285"), for: .normal)
                        ImagecropButton.addTarget(self, action: #selector (cropButtonTapped(_:)), for: .touchUpInside)
                        ImagecropButton.isUserInteractionEnabled = true
                        self.imageSelected = imageView.image
                        
                        let dottedBorder = CAShapeLayer()
                        dottedBorder.strokeColor = UIColor.lightGray.cgColor
                        dottedBorder.lineWidth = 2
                        dottedBorder.lineDashPattern = [10,5]
                        dottedBorder.frame = imageView.bounds
                        dottedBorder.fillColor = nil
                        dottedBorder.name = "dottedBorder"
                        dottedBorder.path = UIBezierPath(rect: imageView.bounds).cgPath
                        imageView.layer.addSublayer(dottedBorder)
                        
                    
                        break
                    }
                }
            }
            else if view is UITextView{
                let dottedBorder = CAShapeLayer()
                dottedBorder.strokeColor = UIColor.lightGray.cgColor
                dottedBorder.lineWidth = 2
                dottedBorder.lineDashPattern = [10,5]
                dottedBorder.frame = view.bounds
                dottedBorder.fillColor = nil
                dottedBorder.name = "dottedBorder"
                dottedBorder.path = UIBezierPath(rect: view.bounds).cgPath
                view.layer.addSublayer(dottedBorder)
                view.becomeFirstResponder()
            }
            else {
                let dottedBorder = CAShapeLayer()
                dottedBorder.strokeColor = UIColor.lightGray.cgColor
                dottedBorder.lineWidth = 2
                dottedBorder.lineDashPattern = [10,5]
                dottedBorder.frame = view.bounds
                dottedBorder.fillColor = nil
                dottedBorder.name = "dottedBorder"
                dottedBorder.path = UIBezierPath(rect: view.bounds).cgPath
                view.layer.addSublayer(dottedBorder)
             
            }
        }
    }
   
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRequireFailureOf otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return false
    }
    
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return false
    }
    
    @objc func screenEdgeSwiped(_ recognizer: UIScreenEdgePanGestureRecognizer) {
        if recognizer.state == .recognized {
            if !stickersVCIsVisible {
                addStickersViewController()
            }
        }
    }
    
  
    override public var prefersStatusBarHidden: Bool {
        return true
    }
    
    
    func scaleEffect(view: UIView) {
        view.superview?.bringSubviewToFront(view)
        
        if #available(iOS 10.0, *) {
            let generator = UIImpactFeedbackGenerator(style: .heavy)
            generator.impactOccurred()
        }
        let previouTransform =  view.transform
        UIView.animate(withDuration: 0.2,
                       animations: {
                        view.transform = view.transform.scaledBy(x: 1.2, y: 1.2)
        },
                       completion: { _ in
                        UIView.animate(withDuration: 0.2) {
                            view.transform  = previouTransform
                        }
        })
    }
   

    func moveView(view: UIView, recognizer: UIPanGestureRecognizer)  {
        
        hideToolbar(hide: true)
        deleteView.isHidden = false
        
        view.superview?.bringSubviewToFront(view)
        let pointToSuperView = recognizer.location(in: self.view)

        view.center = CGPoint(x: view.center.x + recognizer.translation(in: canvasImageView).x,
                              y: view.center.y + recognizer.translation(in: canvasImageView).y)
        
        recognizer.setTranslation(CGPoint.zero, in: canvasImageView)
        
        if let previousPoint = lastPanPoint {
          
            if deleteView.frame.contains(pointToSuperView) && !deleteView.frame.contains(previousPoint) {
                if #available(iOS 10.0, *) {
                    let generator = UIImpactFeedbackGenerator(style: .heavy)
                    generator.impactOccurred()
                }
                UIView.animate(withDuration: 0.3, animations: {
                    view.transform = view.transform.scaledBy(x: 0.25, y: 0.25)
                    view.center = recognizer.location(in: self.canvasImageView)
                })
            }
             
            else if deleteView.frame.contains(previousPoint) && !deleteView.frame.contains(pointToSuperView) {
             
                UIView.animate(withDuration: 0.3, animations: {
                    view.transform = view.transform.scaledBy(x: 4, y: 4)
                    view.center = recognizer.location(in: self.canvasImageView)
                })
            }
        }
        lastPanPoint = pointToSuperView
        
        if recognizer.state == .ended {
            imageViewToPan = nil
            lastPanPoint = nil
            hideToolbar(hide: false)
            deleteView.isHidden = true
            let point = recognizer.location(in: self.view)
            
            if deleteView.frame.contains(point) { // Delete the view
                view.removeFromSuperview()
                if #available(iOS 10.0, *) {
                    let generator = UINotificationFeedbackGenerator()
                    generator.notificationOccurred(.success)
                }
            } else if !canvasImageView.bounds.contains(view.center) {
                UIView.animate(withDuration: 0.3, animations: {
                    view.center = self.canvasImageView.center
                })
                
            }
        }
    }
    
    func subImageViews(view: UIView) -> [UIImageView] {
        var imageviews: [UIImageView] = []
        for imageView in view.subviews {
            if imageView is UIImageView {
                imageviews.append(imageView as! UIImageView)
            }
        }
        return imageviews
    }
}

