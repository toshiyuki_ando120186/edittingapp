//
//  PhotoEditor+Drawing.swift
//  Photo Editor
//
//  Created by Mohamed Hamed on 6/16/17.
//
//
import UIKit




extension PhotoEditorViewController {
    
    override public func touchesBegan(_ touches: Set<UITouch>,
                                      with event: UIEvent?){
        if isDrawing {
            swiped = false
            if let touch = touches.first {
                lastPoint = touch.location(in: self.brushImageview)
                x_max = lastPoint.x
                y_max = lastPoint.y
                x_min = lastPoint.x
                y_min = lastPoint.y
            }
        }
        
        else if stickersVCIsVisible == true {
            if let touch = touches.first {
                let location = touch.location(in: self.view)
                if !stickersViewController.view.frame.contains(location) {
                    removeStickersView()
                }
            }
        }
        
    }
    
    override public func touchesMoved(_ touches: Set<UITouch>,
                                      with event: UIEvent?){
        if isDrawing {
            // 6
            swiped = true
            
            if let touch = touches.first {
                let currentPoint = touch.location(in: brushImageview)
                x_max = x_max > currentPoint.x ? x_max : currentPoint.x
                y_max = y_max > currentPoint.y ? y_max : currentPoint.y
                
                x_min = x_min < currentPoint.x ? x_min : currentPoint.x
                y_min = y_min < currentPoint.y ? y_min : currentPoint.y
                
                drawLineFrom(lastPoint, toPoint: currentPoint)
                
                // 7
                lastPoint = currentPoint
            }
        }
    }
    
    override public func touchesEnded(_ touches: Set<UITouch>,
                                      with event: UIEvent?){
        if isDrawing {
            
           
            
            
           
            let imagge = brushImageview.toImage()
            let cropRect = CGRect(x: x_min * imagge.scale - brushWidth , y: y_min * imagge.scale - brushWidth , width: (x_max - x_min) * imagge.scale + brushWidth * 2 , height: (y_max - y_min) * imagge.scale + brushWidth * 2 )
            
            let contextImage : UIImage = UIImage(cgImage: imagge.cgImage!)
            let imagelef : CGImage = imagge.cgImage!.cropping(to: cropRect)!
            
            //// croped image
            let zzz : UIImage = UIImage(cgImage: imagelef, scale: imagge.scale, orientation: imagge.imageOrientation)
           
            
            let bowl = UIView()
            self.canvasImageView.addSubview(bowl)
            bowl.frame = CGRect(x: x_min - brushWidth / 2, y: y_min - brushWidth / 2, width: x_max - x_min + brushWidth , height: y_max - y_min + brushWidth)
            
            
            
            let extra = UIImageView()
            bowl.addSubview(extra)
            
            extra.contentMode = .scaleAspectFit
           
            extra.frame = CGRect(x: 0, y: 0, width: x_max - x_min + brushWidth, height: y_max - y_min + brushWidth)
            let imageData  = zzz.pngData() as! NSData
            
            let strBase64 = imageData.base64EncodedString(options: .lineLength76Characters)
        
            let dataDecoded = Data(base64Encoded: strBase64, options: .ignoreUnknownCharacters)
            let decodedimage = UIImage(data: dataDecoded!  )
            
            extra.image = decodedimage
            bowl.tag = tag_val
            rotation_info[String(tag_val)] = 0.0
            widthInfo[String(tag_val)] = bowl.frame.width
            heightInfo[String(tag_val)] = bowl.frame.height
            scale_info[String(tag_val)] = 1.0
            tag_val = tag_val + 1 
            addGestures(view: bowl)
            brushImageview.image = nil
           
            if !swiped {
                
                drawLineFrom(lastPoint, toPoint: lastPoint)
            }
        }
        
    }
    
    func drawLineFrom(_ fromPoint: CGPoint, toPoint: CGPoint) {
       
        let canvasSize = brushImageview.frame.integral.size
       
        UIGraphicsBeginImageContextWithOptions(canvasSize, false, 0)
      
        if let context = UIGraphicsGetCurrentContext() {
            brushImageview.image?.draw(in: CGRect(x: 0, y: 0, width: canvasSize.width, height: canvasSize.height))
            // 2
            context.move(to: CGPoint(x: fromPoint.x, y: fromPoint.y))
            context.addLine(to: CGPoint(x: toPoint.x, y: toPoint.y))
            // 3
            context.setLineCap( CGLineCap.round)
            context.setLineWidth(brushWidth)
        
            context.setStrokeColor(drawColor.cgColor)
            context.setBlendMode( CGBlendMode.normal)
            // 4
            context.strokePath()
            // 5
            brushImageview.image = UIGraphicsGetImageFromCurrentImageContext()
          
        }
        UIGraphicsEndImageContext()
    }
}
