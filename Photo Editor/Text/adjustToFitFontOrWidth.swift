//
//  adjustToFitFontOrWidth.swift
//  Photo Editor
//
//  Created by Jadequeline on 20/12/18.
//  Copyright © 2018 Mohamed Hamed. All rights reserved.
//

import Foundation
import UIKit

protocol adjustFontSizeToFillRectProtocol {
    
    func adjustFontSizeToFillRect(_ newBounds: CGRect, view: JLStickerLabelView, labelView: JLAttributedTextView) -> Void
    func adjustsWidthToFillItsContens(_ view: JLStickerLabelView, labelView: JLAttributedTextView) -> Void
    
}

extension adjustFontSizeToFillRectProtocol {
    func adjustFontSizeToFillRect(_ newBounds: CGRect, view: JLStickerLabelView, labelView: JLAttributedTextView) {
        var mid: CGFloat = 0.0
        var stickerMaximumFontSize: CGFloat = 200.0
        var stickerMinimumFontSize: CGFloat = 15.0
        var difference: CGFloat = 0.0
        
        var tempFont = UIFont(name: view.labelTextView.fontName, size: view.labelTextView.fontSize)
        var copyTextAttributes = labelView.textAttributes
        copyTextAttributes[NSAttributedString.Key.font] = tempFont
        var attributedText = NSAttributedString(string: view.labelTextView.text, attributes: copyTextAttributes)
        
        while stickerMinimumFontSize <= stickerMaximumFontSize {
            mid = stickerMinimumFontSize + (stickerMaximumFontSize - stickerMinimumFontSize) / 2
            tempFont = UIFont(name: view.labelTextView.fontName, size: CGFloat(mid))!
            copyTextAttributes[NSAttributedString.Key.font] = tempFont
            attributedText = NSAttributedString(string: view.labelTextView.text, attributes: copyTextAttributes)
            
            difference = newBounds.height - attributedText.boundingRect(with: CGSize(width: newBounds.width - 24, height: CGFloat.greatestFiniteMagnitude), options: [.usesLineFragmentOrigin, .usesFontLeading], context: nil).height
            
            if (mid == stickerMinimumFontSize || mid == stickerMaximumFontSize) {
                if (difference < 0) {
                    view.labelTextView.fontSize = mid - 1
                    return
                }
                
                view.labelTextView.fontSize = mid
                return
            }
            
            if (difference < 0) {
                stickerMaximumFontSize = mid - 1
            }else if (difference > 0) {
                stickerMinimumFontSize = mid + 1
            }else {
                view.labelTextView.fontSize = mid
                return
            }
        }
        
        view.labelTextView.fontSize = mid
        return
    }
    
    func adjustsWidthToFillItsContens(_ view: JLStickerLabelView, labelView: JLAttributedTextView) {
        
        
        let attributedText = labelView.attributedText
        
        let recSize = attributedText?.boundingRect(with: CGSize(width: CGFloat.greatestFiniteMagnitude, height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, context: nil)
        
        //let width1 =
        let w1 = (ceilf(Float((recSize?.size.width)!)) + 25 < 50) ? view.labelTextView.bounds.size.width : CGFloat(ceilf(Float((recSize?.size.width)!)) + 25)
        //let w1 : CGFloat = 200.0
        let h1 = (ceilf(Float((recSize?.size.height)!)) + 25 < 50) ? 50 : CGFloat(ceilf(Float((recSize?.size.height)!)) + 25)
//        let fff = 1
//        if w1 > (UIScreen.main.bounds.size.width - 50) {
//            if hei_label[String(labelView.tag)] != 0 {
//        labelView.text = labelView.text + "\n"
//                hei_label[String(labelView.tag)] = 0
//            }
//
//        }
        var viewFrame = view.bounds
       // print(w1)
      //  print(h1)
     //   print(labelView.text)
//        widthInfo[String(view.tag)] = w1 + 32
//        heightInfo[String(view.tag)] = h1 + 32
        viewFrame.size.width = w1 + 32
        viewFrame.size.height = h1 + 20
        viewFrame.size.width = 200
        viewFrame.size.height = h1 + 32
        view.bounds = viewFrame
        labelView.bounds = viewFrame
       // labelView.lineSpacing = 20
        
        
    }
    
}

