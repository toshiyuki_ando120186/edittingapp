//
//  StartView.swift
//  Photo Editor
//
//  Created by zhezui on 2019/1/22.
//  Copyright © 2019 Mohamed Hamed. All rights reserved.
//

import UIKit

class StartView: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        startbtn.layer.cornerRadius = 10
    
    }
    

    @IBOutlet weak var startbtn: UIButton!
    
    @IBAction func StartButton(_ sender: Any) {
        
        commonClass().GotoViewController(destViewID: "PhotoEditorViewController")
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
