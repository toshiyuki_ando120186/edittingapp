//
//  Post.swift
//  PinchToZoom
//
//  Created by zhezui on 2019/1/28.
//  Copyright © 2019 appcoda. All rights reserved.
//

import Foundation
struct Post {
    var imageUrl:String!
    var caption:String!
}
