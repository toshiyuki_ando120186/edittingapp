//
//  EEpinch.swift
//  Photo Editor
//
//  Created by zhezui on 2019/1/31.
//  Copyright © 2019 Mohamed Hamed. All rights reserved.
//

import UIKit

class EEpinch: UIViewController , UIGestureRecognizerDelegate{

    @IBOutlet weak var tableView: UITableView!
    var posts:[Post] = []
    
    let heightArray : [CGFloat ] = [1 , 1 , 0.7 , 0.8 , 1.5 , 2 ,0.51  , 1 , 1 , 0.9 , 1 , 1, 1, 1, 2, 2.1, 1, 1, 1, 1, 1, 1, 1, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5]
    var allInfo : Array<Substring>!
    var stickersVCIsVisible = false
    var drawColor: UIColor = UIColor.black
    var textColor: UIColor = UIColor.white
    var isDrawing: Bool = false
    var lastPoint: CGPoint!
    var swiped = false
    var lastPanPoint: CGPoint?
    var lastTextViewTransform: CGAffineTransform?
    var lastTextViewTransCenter: CGPoint?
    var lastTextViewFont:UIFont?
    var activeTextView: UITextView?
    var imageViewToPan: UIImageView?
    var isTyping: Bool = false
    var flag = [ String : CGFloat]()
    let currentContext = UIGraphicsGetCurrentContext()
    var eee = 100
    var PostedData = [ String : [Substring] ]()
    var pagecount : Int!
    override func viewDidLoad() {
        super.viewDidLoad()
       
        for _ in 0...10 {
            let post = Post(imageUrl: "exampleURL", caption: "This is supposed to be a long description like an instagram post caption with #hashtags and emojis 😘😊😅🙈")
            posts.append(post)
        }
        setUpTableView()
        //  self.tableView.reloadData()
    }
    func setUpTableView() {
        let allInfo_local = FinalData.split(separator: "#")
        let count = allInfo_local.count
        pagecount = count
        for i in 0 ..< count {
            let infoArray = allInfo_local[i].split(separator:"$")
            PostedData[String(i)] = infoArray
        }
        allInfo = allInfo_local
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 80.0
        tableView.rowHeight = UITableView.automaticDimension
        tableView.separatorColor = UIColor.clear
        tableView.backgroundColor = UIColor.white
        
    }
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @IBAction func clearAction(_ sender: Any) {
        FinalData = ""
        //allInfo.removeAll()
        PostedData.removeAll()
        pagecount = 0
        tableView.reloadData()
    }
}
extension EEpinch: ZoomingDelegate {
    func pinchZoomHandlerStartPinching() {
        print("pinchZoomHandlerStartPinching")
    }
    
    func pinchZoomHandlerEndPinching() {
        print("pinchZoomHandlerEndPinching")
    }
}
extension EEpinch:UITableViewDataSource,UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pagecount
    }
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PostCell1
        // cell.clipsToBounds = false
        for subview in cell.postImagePan.subviews {
            if subview.tag != 1 {
                subview.removeFromSuperview()
            }
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PostCell1
       // cell.setConstraint(const: heightArray[indexPath.row])
        let panheight = CGFloat( Double( (PostedData[String(indexPath.row)])![7])!)
        let heightRatio = panheight / UIScreen.main.bounds.width
        cell.setConstraint(const: heightRatio)
        cell.postImagePan.isUserInteractionEnabled = true
        cell.selectionStyle = .none
        cell.postImagePan.tag = 1
        
        let divide_count : Int = (PostedData[String(indexPath.row)]?.count)! / 8
        
        for i in 0 ..< divide_count {
            
            cell.postImagePan.tag = 0
            let str  = String((PostedData[String(indexPath.row)])![i * 8 + 0])
            let dataDecoded = Data(base64Encoded: str, options: .ignoreUnknownCharacters)
            let decodedimage = UIImage(data: dataDecoded!  )
            //imageView.image = decodedimage
            let imageView = UIImageView(image: decodedimage)
           
            
            cell.postImagePan.addSubview(imageView)
            imageView.tag = eee
            
            imageView.contentMode = .scaleAspectFit
            
            if let width  = Double( (PostedData[String(indexPath.row)])![i * 8 + 4]){
                if let height  = Double( (PostedData[String(indexPath.row)])![i * 8 + 3]){
                    flag[String(eee)] = CGFloat(height)
                    imageView.frame.size = CGSize(width: CGFloat(width), height: CGFloat(height))
                } else{ }
            } else{ }
            
            if let x = Double( (PostedData[String(indexPath.row)])![i * 8 + 1]){
                if let y = Double( (PostedData[String(indexPath.row)])![i * 8 + 2]){
                    imageView.center = CGPoint(x: CGFloat(x), y: CGFloat(y))
                } else { }
                
            } else { }
            
            
            if let rotation = Double( (PostedData[String(indexPath.row)])![i * 8 + 5]){
                imageView.transform = imageView.transform.rotated(by: CGFloat(rotation))
            } else {}
            if let scale_in = Double( (PostedData[String(indexPath.row)])![i * 8 + 6]){
                imageView.transform = imageView.transform.scaledBy(x: CGFloat(scale_in), y: CGFloat(scale_in))
            } else {}
            
//            if let panHeight = Double( (PostedData[String(indexPath.row)])![i * 8 + 7]){
//                imageView.transform = imageView.transform.scaledBy(x: CGFloat(scale_in), y: CGFloat(scale_in))
//            } else {}
            
            eee = eee + 1
            
            let tapgesture = UITapGestureRecognizer(target: self, action: #selector(tapGesture(_:)))
            
            imageView.addGestureRecognizer(tapgesture)
            imageView.isUserInteractionEnabled = true
            
            
        }
        
        
        return cell
    }
    
    @objc func tapGesture(_ recognizer: UITapGestureRecognizer ) {
        
        if let absPoint = recognizer.view?.convert((recognizer.view?.bounds)!, to: self.view){
            
            if let view = recognizer.view {
                if view is UIImageView{
                    
                    //scaleEffect(view: view)
                    let showView = UIView()
                    self.view.addSubview(showView)
                    showView.tag = 33
                    
                    showView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
                    showView.backgroundColor = UIColor.black
                    showView.alpha = 0.0
                    
                    
                    
                    let copyImage = view.toImage()
                    let copyView = UIImageView(image: copyImage)
                    self.view.addSubview(copyView)
                    copyView.tag = 34
                    
                    let width = view.frame.width * (UIScreen.main.bounds.size.width - 50) / view.frame.width
                    let height = view.frame.height * (UIScreen.main.bounds.size.width - 50) / view.frame.width
                    
                    copyView.frame = absPoint
                    
                    
                    UIView.animate(withDuration: 0.5, animations: {
                        showView.alpha = 0.7
                        copyView.frame.size = CGSize(width: width, height: height)
                        copyView.center = CGPoint(x: UIScreen.main.bounds.width / 2.0, y: UIScreen.main.bounds.height / 2.0)
                    })
                    
                    showView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(removeView)))
                }
                
                
            }
        }
    }
    @objc func removeView(){
        for view in self.view.subviews{
            if view.tag == 33 || view.tag == 34 {
                UIView.animate(withDuration: 0.5, animations: {
                    view.removeFromSuperview()
                })
                
            }
        }
    }
    
}


class PostCell1: UITableViewCell {
    
    @IBOutlet weak var postImagePan: EEZoomableImageView! {
        didSet {
            postImagePan.minZoomScale = 1
            postImagePan.maxZoomScale = 4.0
            postImagePan.resetAnimationDuration = 0.5
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = .clear
        //Important!!!!!!
        //self.clipsToBounds = true
        self.selectionStyle = .blue
       // setUpView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    //    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
    internal var aspectConstraint : NSLayoutConstraint? {
        didSet {
            if oldValue != nil {
                postImagePan.removeConstraint(oldValue!)
            }
            if aspectConstraint != nil {
                postImagePan.addConstraint(aspectConstraint!)
            }
        }
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        aspectConstraint = nil
    }
    func setConstraint(const : CGFloat) {
        
        let aspect = const
        
        let constraint = NSLayoutConstraint(item: postImagePan, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: postImagePan, attribute: NSLayoutConstraint.Attribute.height, multiplier: 1 / aspect, constant: 0.0)
        constraint.priority = UILayoutPriority(rawValue: 999)
        aspectConstraint = constraint
        
    }
    
    
   
}
